const ActionType = {
  ADD_POST: 'thread/add-post',
  DELETE_POST: 'thread/delete-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  RESET_CREATED: 'thread/reset-created',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  REACT: 'thread/react',
  COMMENT: 'thread/comment'
};

export { ActionType };

