import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as threadActions from './actions';
import { ActionType } from './common';

const initialState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true,
  updating: false,
  created: false
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(threadActions.loadPosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });

  builder.addCase(threadActions.loadMorePosts.pending, state => {
    state.hasMorePosts = null;
  });

  builder.addCase(threadActions.loadMorePosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });

  builder.addCase(threadActions.toggleExpandedPost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });

  builder.addCase(ActionType.RESET_CREATED, state => {
    state.created = false;
  });

  builder.addMatcher(
    isAnyOf(threadActions.likePost.fulfilled, threadActions.dislikePost.fulfilled, threadActions.addComment.fulfilled),
    (state, action) => {
      const { posts, expandedPost } = action.payload;
      state.posts = posts;
      state.expandedPost = expandedPost;
      state.updating = false;
    }
  );

  builder.addMatcher(
    isAnyOf(threadActions.dislikePost.pending, threadActions.likePost.pending),
    state => {
      state.updating = true;
    }
  );

  builder.addMatcher(isAnyOf(
    threadActions.applyPost.fulfilled,
    threadActions.createPost.fulfilled
  ), state => {
    state.created = true;
  });

  builder.addMatcher(isAnyOf(
    threadActions.deletePost.fulfilled
  ), (state, action) => {
    const { postId } = action.payload;
    state.posts = state.posts.filter(post => post.id !== postId);
  });
});

export { reducer };
