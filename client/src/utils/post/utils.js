
const reduceOppositeCount = count => {
  if (count < 1) {
    return count;
  }
  return count - 1;
};

const isAuthor = (userId, postCreatorId) => userId === postCreatorId;

export default { reduceOppositeCount, isAuthor };
