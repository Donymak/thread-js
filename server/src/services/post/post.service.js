class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    if (filter.isLike) {
      return this._postRepository.getPostsLikedByUser(filter);
    }
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  deletePostById(id) {
    return this._postRepository.deleteById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  getReactionsCount(postId) {
    return this._postReactionRepository.getReactionsCount(postId);
  }

  async setReaction(userId, { postId, isLike = true }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? this._postRepository.getPostById(postId)
      : this._postRepository.getPostById(postId);
  }

  deleteReactionsOfDeletingPost(postId) {
    return this._postReactionRepository.deleteReactionsOfDeletingPost(postId);
  }
}

export { Post };
