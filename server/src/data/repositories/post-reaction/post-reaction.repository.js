import { Abstract } from '../abstract/abstract.repository';
import { getReactionsQuery } from '../post/helpers';

class PostReaction extends Abstract {
  constructor({ postReactionModel }) {
    super(postReactionModel);
  }

  getPostReaction(userId, postId) {
    return this.model.query()
      .select()
      .where({ userId })
      .andWhere({ postId })
      .withGraphFetched('[post]')
      .first();
  }

  getReactionsCount(postId) {
    return this.model.query()
      .select(
        'post.',
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where({ postId });
  }

  deleteReactionsOfDeletingPost(postId) {
    return this.model.query()
      .select(
        'posts_reactions.*'
      )
      .where({ postId })
      .delete();
  }
}

export { PostReaction };
